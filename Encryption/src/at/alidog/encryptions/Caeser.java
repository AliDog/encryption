package at.alidog.encryptions;

public class Caeser{

	public String encrypt(String message, int key) {
		
				String encryptedMessage = "";
				char ch;
		 
				for(int i = 0; i < message.length(); ++i){
					ch = message.charAt(i);
					
					if(ch >= 'a' && ch <= 'z'){
			            ch = (char)(ch + key);
			            
			            if(ch > 'z'){
			                ch = (char)(ch - 26);
			            }
			            
			            encryptedMessage += ch;
			        }
			        else if(ch >= 'A' && ch <= 'Z'){
			            ch = (char)(ch + key);
			            
			            if(ch > 'Z'){
			                ch = (char)(ch - 26);
			            }
			            
			            encryptedMessage += ch;
			        }
			        else {
			        	encryptedMessage += ch;
			        }
				}
				
				return encryptedMessage;
			}


	public String decrypt(String message, int key) {
		
		String decryptedMessage = "";
		char ch;
 
		for(int i = 0; i < message.length(); ++i){
			ch = message.charAt(i);
			
			if(ch >= 'a' && ch <= 'z'){
	            ch = (char)(ch - key);
	            
	           if(ch < 'a'){
	                ch = (char)(ch + 26);
	            }
	            
	            decryptedMessage += ch;
	        }
	        else if(ch >= 'A' && ch <= 'Z'){
	            ch = (char)(ch - key);
	            
	            if(ch < 'A'){
	                ch = (char)(ch + 26);
	            }
	            
	            decryptedMessage += ch;
	        }
	        else {
	        	decryptedMessage += ch;
	        }
		}
		
		return decryptedMessage;
	}

}
