package at.alidog.encryptions;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	
		Caeser text = new Caeser();
		String message = new String();
		Scanner sc = new Scanner(System.in);
		Boolean check = true;
		
		while(check) {
			System.out.println("Geben Sie Ihre Nachricht ein");
			message = sc.nextLine();
			char ch;
			
			for(int i = 0; i < message.length(); ++i){
			ch = message.charAt(i);
			
				if(ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z') {
					
					check = false;
					
				}else {
					System.out.println("\nUngültige Eingabe!\n");
					check = true;
					break;
				}
			}
		}
		
		System.out.println("\nGeben Sie den Schlüssel ein");
		int key = (int)sc.nextFloat();
		
		message = text.encrypt(message, key);
		
		System.out.println("\n\nVerschlüsselte Nachricht: " + message);
		
		message = text.decrypt(message, key);
		
		System.out.println("\nUnverschlüsselte Nachricht: " + message);
		
	}

}
