package at.alidog.encryptions;

public interface Encryption {

	public String encrypt(String string);
	public String decrypt(String string);
}
